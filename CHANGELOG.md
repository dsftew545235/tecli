# CHANGELOG

## Bugfix

Fix access levels for commands: workspace.

## Feature

Add new option to update workspace variable by using its keys.
